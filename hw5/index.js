// Создается обьект с свойствами и вложенным обьектом
// сначала отрабатывает вызов doc.content(); 
// после ввода данных вызывается doc.docWrite();
const doc = {
    head: "",
    body: "",
    foot: "",
    date: "",
    app: {
        head: "",
        body: "",
        foot: "",
        date: "",
    },
    // метод ввода данных
    content: function () {
        this.head = prompt("Введите заголовок!");
        this.app.head = prompt("Введите app заголовок!");
        this.body = prompt("Введите тело!");
        this.app.body = prompt("Введите app тело!");
        this.foot = prompt("Введите футер!");
        this.app.foot = prompt("Введите app футер!");
        this.date = prompt("Введите дату!");
        this.app.date = prompt("Введите app дату!");
    },
    // метод вывода данных
    docWrite: function () {
        document.write(`<div class="header"><p class="headerMain">Заголовок документа ${this.head}</p><p class="headerApp">App Заголовок документа ${this.app.head}</p></div>`);
        document.write(`<div class="main"><p class="bodyMain">Тело документа ${this.body}</p><p class="bodyApp">App Тело документа ${this.app.body}</p></div>`);
        document.write(`<div class="footer"><p class="footerMain">Футер документа ${this.foot}</p><p class="footerApp">App Футер документа ${this.app.foot}</p></div>`);
        document.write(`<div class="date"><p class="dateMain">Дата документа ${this.date}</p><p class="dateApp">App Дата документа ${this.app.date}</p></div>`);
    }
};
// вызов функций
doc.content();
doc.docWrite();