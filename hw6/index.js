//  Задание 1.1

// function Human(name, age) {
//     this.name = name;
//     this.age = age;
// }

// const igor = new Human('Igor', 28);
// const alex = new Human('Alex', 22);
// const irina = new Human('Irina', 74);
// const anya = new Human('Anya', 25);
// const sergei = new Human('Sergei', 36);

// const persons = [igor, alex, irina, anya, sergei];

// document.write('<p>Ages</p>')
// for (i = 0; i < persons.length; i++) {
//     document.write(`| ${persons[i].name} ${persons[i].age} `);
// }

// document.write('<p>Ages Возрастание</p>')
// persons.sort((a, b) => a.age - b.age);
// for (i = 0; i < persons.length; i++) {
//     document.write(`| ${persons[i].name} ${persons[i].age} `);
// }

// document.write('<p>Ages Убывание</p>')
// persons.sort((a, b) => b.age - a.age);
// for (i = 0; i < persons.length; i++) {
//     document.write(`| ${persons[i].name} ${persons[i].age} `);
// }

// Задание 1.2
// Сортировка по среднему баллу

// Добавил новое свойство "Вычисление среднего балла"
function Human(name) {
    this.name = name;
    this.lesson1 = Math.floor(Math.random() * Math.floor(7));
    this.lesson2 = Math.floor(Math.random() * Math.floor(7));
    this.lesson3 = Math.floor(Math.random() * Math.floor(7));
    this.lesson4 = Math.floor(Math.random() * Math.floor(7));
    this.lesson5 = Math.floor(Math.random() * Math.floor(7));
    this.avg = Math.round((this.lesson1 + this.lesson2 + this.lesson3 + this.lesson4 + this.lesson5) / 5);
    this.task = Math.floor(Math.random() * Math.floor(100));
}

const igor = new Human('Igor');
const alex = new Human('Alex');
const irina = new Human('Irina');
const anya = new Human('Anya');
const sergei = new Human('Sergei');

const persons = [igor, alex, irina, anya, sergei];
// Происходит сортировка по среднему баллу, рассположение и цвет от балла  
persons.sort((a, b) => b.avg - a.avg);
for (i = 0; i < persons.length; i++) {
    if (persons[i].avg >= 5) {
        document.write(`<div class="high">${persons[i].name} Ваш средний балл ${persons[i].avg} <br>Так держать, переходите к новым задачам!</div>`);
    } else if (persons[i].avg >= 3) {
        document.write(`<div class="medium">${persons[i].name} Ваш средний балл ${persons[i].avg} <br>Проведите анализ своих ошибок</div>`);
    } else {
        document.write(`<div class="low">${persons[i].name} Ваш средний балл ${persons[i].avg} <br>Вернитесь к базовой теории</div>`);
    }
}