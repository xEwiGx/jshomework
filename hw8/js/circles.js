const btn = document.querySelector('#btn');
btn.style.display = 'block';
btn.style.margin = '5px auto';
// btn.style.background = rColor();
btn.style.padding = '10px'
btn.style.borderRadius = '10px';
// по клику на #btn создаем поле input в который можно ввести диаметр и кнопку для вывода кругов 
btn.onclick = function () {
    const diameter = document.createElement('input');
    diameter.classList.add('diametr');
    diameter.type = 'text';
    diameter.value = '30';
    diameter.style.borderRadius = '50%';
    diameter.style.outline = 'none';
    diameter.style.width = '50px';
    diameter.style.height = '50px';
    diameter.style.fontSize = '2em'
    diameter.style.textAlign = 'center';
    diameter.style.display = 'block';
    diameter.style.margin = '5px auto';
    const circlesBtn = document.createElement('button');
    circlesBtn.style.padding = '10px'
    circlesBtn.style.display = 'block';
    circlesBtn.style.margin = '5px auto 20px';
    circlesBtn.textContent = 'Нарисовать';
    // circlesBtn.style.background = rColor();
    circlesBtn.style.borderRadius = '10px';
    document.body.appendChild(diameter);
    document.body.appendChild(circlesBtn);
    // При нажатии на 'Нарисовать' получим значение введенное в input, создадим обертку для кругов вызовем функции
    circlesBtn.onclick = function () {
        const dValue = +diameter.value;
        const circleWrapper = document.createElement('div');
        circleWrapper.style.margin = '0 auto';
        circleWrapper.style.width = `${dValue * 10}px`;
        document.body.appendChild(circleWrapper);
        // функция создания круга
        function addCircle() {
            const circle = document.createElement('div');
            circle.classList.add('item');
            circle.style.background = rColor();
            circle.style.width = `${dValue}px`;
            circle.style.height = `${dValue}px`;
            circle.style.display = 'inline-block';
            circle.style.borderRadius = '50%';
            circleWrapper.appendChild(circle);
        }
        // запускаем функцию addCircle 100раз
        for (i = 0; i < 100; i++) {
            let a = new addCircle();
        }
        // удаляем круг при клике
        const circleArr = document.querySelectorAll('.item');
        circleArr.forEach(element => {
            element.onclick = function () {
                this.remove();
            }
        });
    }
}

// рандомайзер цвета
function rColor() {
    let n = (Math.random() * 0xfffff * 1000000).toString(16);
    return '#' + n.slice(0, 6);
}
rColor();