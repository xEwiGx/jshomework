// прогружаем сначала скрипт потом запускаемы
/* document.addEventListener('DOMContentLoader', () => { */
window.onload = () => {
    const flagsLeft = document.querySelector('#flags-left')
    const result = document.querySelector('#result')
    const grid = document.querySelector('.grid')
    let width = 8
    let bombAmount = 12
    let squares = []
    let isGameOver = false
    let flags = 0
    // create Board

    function createBoard() {

        flagsLeft.innerHTML = bombAmount
        // создаем 20 обьектов массива с class="bomb" 
        const bombsArray = Array(bombAmount).fill('bomb')
        // создаем 80 обьектов массива с class="valid" 
        const emptyArray = Array(width * width - bombAmount).fill('valid')
        // console.log(bombsArray)
        // console.log(emptyArray)
        // обьединяем массивы в один
        const gameArray = emptyArray.concat(bombsArray)
        // console.log(gameArray)
        //добавляем метод сортировки для получения рандомного индекса расположения элементов
        const shuffledArray = gameArray.sort(() => Math.random() - 0.5)
        // console.log(shuffledArray)
        // так как у нас сетка 400*400 с блоками 40*40 ,цикл отраблотает 100раз
        for (let i = 0; i < width * width; i++) {
            // создаем 100 div
            const square = document.createElement('div')
            // задаем id=[i](от 0 до 99) каждому новому div
            square.setAttribute('id', i)
            //в соответсвии с рандомным индексом расположения обьектов, поочередно будет добавляться класс либо .bomb либо .valid
            square.classList.add(shuffledArray[i])
            // добавляем div(узел) в качестве последнего дочернего узла в grid
            grid.appendChild(square)
            // за div(узел) начнут последовательно выстраиваться остальные div  
            squares.push(square)

            // при клике левой кнопки на див(квадрат) вызываем функцию с соответсвующим свойством
            square.addEventListener('click', function (e) {
                click(square)
            })
            // при клике правой кнопки на див(квадрат) вызываем функцию с соответсвующим свойством
            square.oncontextmenu = function (e) {
                e.preventDefault()
                addFlag(square)
            }
        }
        /* 

    |0 |1 |2 |3 |4 |5 |6 |7 |  
    |8 |9 |10|11|12|13|14|15|
    |16|17|18|19|20|21|22|23|
    |24|25|26|27|28|29|30|31|
    |32|33|34|35|36|37|38|39|
    |40|41|42|43|44|45|46|47|
    |48|49|50|51|52|53|54|55|
    |56|57|58|59|60|61|62|63|
   
    */
        //add numbers
        for (let i = 0; i < squares.length; i++) {
            let total = 0
            // если остаток от деления 0 то мы находимся у левой границы
            const isLeftEdge = (i % width === 0)
            // если остаток от деления 9 то мы находимся у правой границы
            const isRightEdge = (i % width === width - 1)


            // если у элемента class='valid' = true то
            if (squares[i].classList.contains('valid')) {

                /* 
                total = +1, если [i]елемента больше 8 и элемент с class='bomb'
                  распологается:
                | 3 | 4b| 5 |
                |13 |14v|15 |
                |23 |24 |25 |
                  */
                if (i > 8 && squares[i - width].classList.contains('bomb')) total++
                /* 
                total = +1, если [i]елемента больше 7 и элемент не находится с правой стороны и элемент с class='bomb'
                  распологается:
                | 3 | 4 |5b |
                |13 |14v|15 |
                |23 |24 |25 |
                  */
                if (i > 7 && !isRightEdge && squares[i + 1 - width].classList.contains('bomb')) total++
                /* 
                total = +1, если [i]елемента меньше 62 и элемент не находится с правой стороны и элемент с class='bomb'
                  распологается:
                | 3 | 4 | 5 |
                |13 |14v|15b|
                |23 |24 |25 |
                  */
                if (i < 62 && !isRightEdge && squares[i + 1].classList.contains('bomb')) total++
                /* 
                total = +1, если [i]елемента меньше 54 и элемент не находится с правой стороны и элемент с class='bomb'
                  распологается:
                | 3 | 4 | 5 |
                |13 |14v|15 |
                |23 |24 |25b|
                  */
                if (i < 54 && !isRightEdge && squares[i + 1 + width].classList.contains('bomb')) total++
                /* 
                total = +1, если [i]елемента меньше 55 и элемент с class='bomb'
                  распологается:
                | 3 | 4 | 5 |
                |13 |14v|15 |
                |23 |24b|25 |
                  */
                if (i < 55 && squares[i + width].classList.contains('bomb')) total++
                /* 
                total = +1, если [i]елемента меньше 56 и элемент не находится с левой стороны и элемент с class='bomb'
                  распологается:
                | 3 | 4 | 5 |
                |13 |14v|15 |
                |23b|24 |25 |
                  */
                if (i < 56 && !isLeftEdge && squares[i - 1 + width].classList.contains('bomb')) total++
                /* 
                 total = +1, если [i]елемента больше 0 и элемент не находится с левой стороны и элемент с class='bomb'
                   распологается:
                 | 3 | 4 | 5 |
                 |13b|14v|15 |
                 |23 |24 |25 |
                   */
                if (i > 0 && !isLeftEdge && squares[i - 1].classList.contains('bomb')) total++
                /* 
                total = +1, если [i]елемента больше 9 и элемент не находится с левой стороны и элемент с class='bomb'
                  распологается:
                | 3b| 4 | 5 |
                |13 |14v|15 |
                |23 |24 |25 |
                  */
                if (i > 9 && !isLeftEdge && squares[i - 1 - width].classList.contains('bomb')) total++

                // добавляем атрибут data в div.valid с соответсвующим total(колличество соприкосаемых бомб)
                squares[i].setAttribute('data', total)
                // console.log(squares)
            }
        }

    }
    createBoard()

    // добавляем флаг
    function addFlag(square) {
        if (isGameOver) return
        if (!square.classList.contains('checked') && (flags < bombAmount)) {
            if (!square.classList.contains('flag')) {
                square.classList.add('flag')
                square.innerHTML = '<i class="fas fa-flag"></i>'
                flags++
                flagsLeft.innerHTML = bombAmount - flags
                checkForWin()
            } else {
                square.classList.remove('flag')
                square.innerHTML = ''
                flags--
                flagsLeft.innerHTML = bombAmount - flags
            }
        }
    }


    // задаем условия при клике
    function click(square) {
        let currentId = square.id
        if (isGameOver) return /* ничего не происходит , возвращаемся */
        if (square.classList.contains('checked') || square.classList.contains('flag')) return /* ничего не происходит , возвращаемся */
        // если нажали на бомбу то Game over
        if (square.classList.contains('bomb')) {
            // alert('Game over')
            // console.log('Game over')
            gameOver(square)
        }
        // если нажал на пустой квадрат у которого data не 0 то возвращаем в документ цифру соответсвующую дате(соприкосаемых бомб) и присваеваем класс 'checked'
        else {
            let total = square.getAttribute('data')
            if (total != 0) {
                square.classList.add('checked')
                if (total == 1) square.classList.add('one')
                if (total == 2) square.classList.add('two')
                if (total == 3) square.classList.add('three')
                if (total == 4) square.classList.add('four')
                square.innerHTML = total
                return
            }
            // Запускаем функцию открытия пустых клеток
            checkSquare(square, currentId)
        } // все остальное присваеваем класс 'checked'
        square.classList.add('checked')
    }
    // Функция открытия пустых клеток 
    function checkSquare(square, currentId) {
        const isLeftEdge = (currentId % width === 0)
        const isRightEdge = (currentId % width === width - 1)
        // создаем условия раскрытия пустых квадратов в стороны вверх, вниз, влево, вправо, с задержкой .
        //  до упора в правую или левую стенку или квадратом id которого не 0 
        setTimeout(() => {

            if (currentId > 0 && !isLeftEdge) {
                const newId = squares[parseInt(currentId) - 1].id
                const newSquare = document.getElementById(newId)
                click(newSquare)
            }
            if (currentId > 7 && !isRightEdge) {
                const newId = squares[parseInt(currentId) + 1 - width].id
                const newSquare = document.getElementById(newId)
                click(newSquare)
            }
            // теперь пустые клетки будут открываться вверх и вправо
            if (currentId > 8) {
                const newId = squares[parseInt(currentId - width)].id
                const newSquare = document.getElementById(newId)
                click(newSquare)
            }
            if (currentId > 9 && !isLeftEdge) {
                const newId = squares[parseInt(currentId) - 1 - width].id
                const newSquare = document.getElementById(newId)
                click(newSquare)
            }
            if (currentId < 62 && !isRightEdge) {
                const newId = squares[parseInt(currentId) + 1].id
                const newSquare = document.getElementById(newId)
                click(newSquare)
            }
            if (currentId < 56 && !isLeftEdge) {
                const newId = squares[parseInt(currentId) - 1 + width].id
                const newSquare = document.getElementById(newId)
                click(newSquare)
            }
            if (currentId < 54 && !isRightEdge) {
                const newId = squares[parseInt(currentId) + 1 + width].id
                const newSquare = document.getElementById(newId)
                click(newSquare)
            }
            if (currentId < 55) {
                const newId = squares[parseInt(currentId) + width].id
                const newSquare = document.getElementById(newId)
                click(newSquare)
            }
        }, 100)
    }

    // gameover funct
    // уведомляем о конце игры и открываем бомбы
    function gameOver(square) {
        result.innerHTML = 'BOOM! Game Over!'
        isGameOver = true

        //show all bomb location
        squares.forEach(square => {
            if (square.classList.contains('bomb')) {
                square.innerHTML = '<i class="fas fa-bomb"></i>'
            }
        })
    }
    // Победа
    function checkForWin() {
        let matches = 0
        for (let i = 0; i < squares.length; i++) {
            if (squares[i].classList.contains('flag') && squares[i].classList.contains('bomb')) {
                matches++
            }

            if (matches === bombAmount) {
                result.innerHTML = 'YOU WIN!'
                isGameOver = true
            }
        }
    }

    document
        .querySelector('button')
        .addEventListener('click', init)

    function init() {
        location.reload(true);
    }

}