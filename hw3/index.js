//----- 1
document.write('<p style="color: red">Задание 1: Создать массив с эл-т Джаз и Блюз</p>');

const style = ['Джаз', 'Блюз'];
document.write('<p>состояние массива: ' + style);

//----- 2
document.write('<p style="color: red">Задание 2: Добавить в конец Рок-н-ролл</p>');

style.push('Рок-н-ролл');
document.write('<p>состояние массива: ' + style);

//----- 3
document.write('<p style="color: red">Задание 3: Заменить значение в середине на Классика,</br> сохранить место в центре при любом размере массива</p>');

style.splice(1, 1); /* Удаляем 'Блюз' */
const x = style.length; /* Задаем переменную колличества эл-в */
const s = x / 2; /* Переменная для определения центра в массиве */
/* Заменяем Блюз на Классика с сохранением центрового места при любом размере массива*/
style.splice(s, 0, "Классика");
document.write('<p>состояние массива: ' + style);
// -------
const style2 = ['1', '2', '3', '4', '4', '3', '2', '1', ];
const r = style2.length;
const g = r / 2;
style2.splice(g, 0, "Классика");
document.write('<p>Пример центрирования: ' + style2);

//----- 4
document.write('<p style="color: red">Задание 4: Удалить первый эл-т и показать его</p>');

const d = style.splice(0, 1);
document.write('<p>Удаленный первый элемент : ' + d);
document.write('<p>состояние массива: ' + style);

//----- 5
document.write('<p style="color: red">Задание 5: Вставить Рэп и Регги в начало</p>');

style.unshift('Рэп', 'Регги');
document.write('<p>состояние массива: ' + style);